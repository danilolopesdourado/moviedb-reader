import 'package:flutter/material.dart';
import 'package:moviedb_reader/dataModels/dataModels.dart';
import 'package:moviedb_reader/ui/pages/movieDetail.dart';

class MovieCard extends StatelessWidget {
  const MovieCard(this.movie);

  final Movie movie;
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Hero(
        tag: movie.id,
        child: Container(
          width: double.infinity,
          margin: EdgeInsets.only(bottom: 15.0),
          child: ClipRRect(                                  
            borderRadius:  BorderRadius.circular(10.0),
            child: Stack(
              children: [
                Image.network(
                  'https://image.tmdb.org/t/p/w500/'+movie.posterPath,
                ),
                Positioned.fill(
                  bottom: 0,
                  child: Container(  
                    height: 162,
                    decoration: BoxDecoration(    
                      borderRadius:  BorderRadius.circular(10.0),
                      gradient: LinearGradient(
                        colors: [
                          Color.fromRGBO(0,0,0,0.0),
                          Color.fromRGBO(0,0,0,1.0),
                        ],
                        begin: FractionalOffset.topCenter,
                        end: FractionalOffset.bottomCenter,
                        stops: [0.0,1.0]
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 56.0,
                  left: 24.0,
                  child: Text(
                    movie.title,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 32.0,
                  left: 24.0,
                  child: Text(
                    movie.movieGenres(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 10,
                    ),
                  ),
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(
            borderRadius:  BorderRadius.circular(10.0),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Color.fromRGBO(0, 56, 76, 0.2),
                blurRadius: 20.0,
                offset: Offset(0,20),
                spreadRadius: -10,
              )
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => MovieDetailPage(movieData: movie),
          ),
        );
      },                   
    );
  }
}

class InfoSquare extends StatelessWidget {
  const InfoSquare(this.infoName, this.infoValue);

  final String infoName;
  final String infoValue;
  
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical:9.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Color(0xfff1f3f5),
      ),
      child: Align(
        alignment: Alignment.centerLeft,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(        
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                this.infoName+' ',
                style: TextStyle(
                  color: Color(0xff868e96),
                  fontWeight: FontWeight.w600,
                  fontSize: 12,
                ),
              ), 
              Text(
                this.infoValue,
                style: TextStyle(
                  color: Color(0xff343a40),
                  fontWeight: FontWeight.w600,
                  fontSize: 14,
                ),

              ),
            ],
          ),
        ),
      ),
    );
  }
}


class InfoLongDescription extends StatelessWidget {
  const InfoLongDescription(this.infoName, this.infoValue);

  final String infoName;
  final String infoValue;
  
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          infoName,
          style: TextStyle(
            color: Color(0xff5e6770),
            fontSize: 14,
          ),
        ),
        SizedBox(height: 8.0,),
        Text(
          infoValue,
          style: TextStyle(
            color: Color(0xff343a40),
            fontWeight: FontWeight.w600,
            fontSize: 12,
          ),
        ),
      ],
    );
  }
}