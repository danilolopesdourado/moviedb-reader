import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:moviedb_reader/apiHandler/apiHandlers.dart';
import 'package:moviedb_reader/dataModels/dataModels.dart';
import 'package:moviedb_reader/ui/customWidgets/customWidgets.dart';
import 'package:moviedb_reader/globals.dart' as globals;

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<Map> futureMovies;
  int _selectedGenre;
  TextEditingController _filterByName;

  @override
  void initState() {
    super.initState();
    _filterByName = TextEditingController();
    futureMovies = fetchMovies('movie/popular');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: FutureBuilder<Map>(
        future: futureMovies,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Movie> _movies = snapshot.data['movieList'];
            List<Genre> _genres = snapshot.data['genreList'];
            return Padding(
              padding: EdgeInsets.fromLTRB(20.0, 48.0, 20.0, 0),
              child: Stack(
                children: [
                  Positioned(
                    top: 127,
                    child: SizedBox(
                      height: MediaQuery.of(context).size.height-200.0,
                      width: MediaQuery.of(context).size.width-40,
                      child: ListView.builder(
                        itemCount: _movies.length,
                        itemBuilder: (context, index) {
                          return  (
                            (_movies[index].genres.firstWhere((genre) => genre.id == _selectedGenre, orElse: () => null) != null || (_selectedGenre == null)) 
                            && 
                            (_movies[index].title.toLowerCase().contains(_filterByName.text.toLowerCase()) || _movies[index].originalTitle.toLowerCase().contains(_filterByName.text.toLowerCase()) || (_filterByName==null))
                          ) 
                          ? MovieCard(_movies[index])
                          : Container();
                        },
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          gradient: LinearGradient(
                            colors: [
                              Color.fromRGBO(255,255,255,1.0),
                              Color.fromRGBO(255,255,255,0),
                            ],
                            begin: FractionalOffset.topCenter,
                            end: FractionalOffset.bottomCenter,
                            stops: [0.5,1.0]

                          ),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text(
                                  widget.title,
                                  style: TextStyle(
                                    color: globals.gray1,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  textAlign: TextAlign.left,
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 24.0,
                            ),
                            TextField(
                              controller: _filterByName,
                              style: TextStyle(
                                color: globals.gray2,
                                fontSize: 14,
                              ),
                              decoration: InputDecoration(
                                contentPadding: EdgeInsets.all(17),
                                prefixIcon: Icon(
                                  Icons.search,
                                  size: 14,
                                  color: globals.gray2,
                                ),
                                hintText: 'Pesquise filmes',
                                border: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      width: 0, 
                                      style: BorderStyle.none,
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(100.0)),
                                ),
                                fillColor: Color(0xfff1f3f5),
                                filled: true,
                              ),
                            ),
                            SizedBox(
                              height: 56.0,
                              child: ListView.separated(
                                separatorBuilder: (BuildContext context, int index) => SizedBox(width: 12.0,),
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: _genres.length,
                                itemBuilder: (context, index) {
                                  return ChoiceChip(
                                    selected: _selectedGenre == _genres[index].id,
                                    padding: EdgeInsets.symmetric(horizontal: 12.0),
                                    labelPadding: EdgeInsets.zero,
                                    backgroundColor: Colors.white,
                                    selectedColor: Color(0xff00384c),
                                    shape: StadiumBorder(
                                      side: BorderSide(
                                        width: 1.0,
                                        color: _selectedGenre == _genres[index].id ? Color(0xff00384c) : Color(0xfff1f3f5),
                                      ),
                                    ),
                                    label: Text(
                                      _genres[index].name??'genero',                      
                                      style: TextStyle(
                                        color: _selectedGenre == _genres[index].id ? Colors.white : Color(0xff00384c),
                                        fontSize: 12,
                                      ),
                                    ),
                                    onSelected: (bool selected) {
                                      setState(() {
                                        _selectedGenre = selected ? _genres[index].id : null;
                                      });
                                    },
                                  );
                                },
                              ),
                            ),
                            SizedBox(
                              height: 48.0,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height:  MediaQuery.of(context).size.height-249.0,
                      ),
                    ],
                  ),
                ],
              ),
            );
          } else if (snapshot.hasError) {
            print(snapshot.error.toString());
            return Center(child:
              snapshot.error.toString().contains('SocketException')
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.wifi_off),
                    Text(' Verifique sua conexão! ', style: TextStyle(color: Colors.black),),
                    Icon(Icons.mobile_off),
                  ],
                )
              : Text("${snapshot.error}")
            );
          }

          return Center(child: CircularProgressIndicator());
        },
      ),
      
    );
  }
}
