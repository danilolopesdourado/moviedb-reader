class Genre {
  final int id;
  final String name;

  Genre(this.id, this.name);

  Genre.fromJson(Map<String, dynamic> json)
      : name = json['name'],
        id = json['id'];

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
    };
}


class Movie {
  final int budget;
  final String cast;
  final String director;
  final List<Genre> genres;
  final int id;
  final String originalTitle;
  final String overview;
  final String posterPath;
  final String productionCompanies;
  final String releaseDate;
  final int runtime;
  final String title;
  final bool video;
  final double voteAverage;
  

  Movie(
    this.budget,
    this.cast,
    this.director,
    this.genres,
    this.id,
    this.originalTitle,
    this.overview,
    this.posterPath,
    this.productionCompanies,
    this.releaseDate,
    this.runtime,
    this.title,
    this.video,
    this.voteAverage,
  );

  Movie.fromJson(Map<String, dynamic> json)
      : budget = json['budget'],
        cast = json['cast'],
        director = json['director'],
        genres = json['genres'].map<Genre>((genreJson) => Genre.fromJson(genreJson)).toList(),
        id = json['id'],
        originalTitle = json['original_title'],
        overview = json['overview'],
        posterPath = json['poster_path'],
        productionCompanies = json['production_companies'][0]['name'],
        releaseDate = json['release_date'],
        runtime = json['runtime'],
        title = json['title'],
        video = json['video'],
        voteAverage = json['vote_average'].toDouble();

  Map<String, dynamic> toJson() =>
    {
      'budget' : budget,
      'director' : director,
      'genres' : genres,
      'id' : id,
      'original_title' : originalTitle,
      'overview' : overview,
      'poster_path' : posterPath,
      'production_companies' : productionCompanies,
      'release_date' : releaseDate,
      'title' : title,
      'runtime' : runtime,
      'video' : video,
      'vote_average' : voteAverage,
    };

  String movieGenres(){
    return this.genres.map((e) => e.name).join(' - ');
  }
}
