import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:moviedb_reader/ui/customWidgets/customWidgets.dart';
import 'package:moviedb_reader/dataModels/dataModels.dart';
import 'package:moviedb_reader/globals.dart' as globals;

class MovieDetailPage extends StatelessWidget {

  MovieDetailPage({Key key, @required this.movieData}) : super(key: key);
  final Movie movieData;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              alignment: Alignment.topCenter,
              overflow: Overflow.visible,
              children: [
                Column(
                  children: [
                    Container(
                      height: 303,
                      decoration: BoxDecoration(
                        color: globals.gray8,
                      ),
                    ),
                    Container(
                      height: 190,
                    ),
                  ],
                ),                
                Positioned(
                  top: 48, 
                  left: 20,
                  child: InkWell(
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 13, vertical: 9),
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                          Icon(
                            Icons.arrow_back_ios,
                            color: globals.gray2_2,
                            size: 12.0,
                          ),
                          Text(
                            'Voltar',
                            style: TextStyle(
                              color: globals.gray2_2,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),            
                      decoration: BoxDecoration(
                        borderRadius:  BorderRadius.all(Radius.circular(100.0)),
                        color: Colors.white,
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            blurRadius: 10.0,
                            offset: Offset(0,10),
                            spreadRadius: -10.0,
                          )
                        ]
                      ),
                    ),
                    onTap: (){ Navigator.of(context).pop(); },
                  ),
                ),
                Positioned(
                  top: 136, 
                  child: Hero(
                    tag: movieData.id,
                    transitionOnUserGestures: true,
                    child: Container(
                      height: 318.0,
                      width: 216,
                      child: ClipRRect(                                  
                        borderRadius:  BorderRadius.circular(10.0),
                        child: Image.network(
                          'https://image.tmdb.org/t/p/w500/'+movieData.posterPath,
                        ),
                      ),
                      decoration: BoxDecoration(
                        borderRadius:  BorderRadius.circular(10.0),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Color.fromRGBO(0, 56, 76, 0.2),
                            blurRadius: 20.0,
                            offset: Offset(0,20),
                            spreadRadius: -10.0,
                          )
                        ]
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.fromLTRB(20.0, 0.0,  20.0, 90.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        movieData.voteAverage.toString(),
                        style: TextStyle(
                          color: Color(0xff00384c),
                          fontWeight: FontWeight.w600,
                          fontSize: 24,
                        ),
                      ),
                      SizedBox(width: 4.0,),
                      Text(
                        '/ 10',
                        style: TextStyle(
                          color: globals.gray3,
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 32,),
                  Center(
                    child:Text(
                      movieData.title.toUpperCase(),
                      style: TextStyle(
                        color: globals.gray3,
                        fontWeight: FontWeight.w600,
                        fontSize: 14,
                      ),
                    ),
                  ),
                  SizedBox(height: 12,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Título original: ',
                        style: TextStyle(
                          color: globals.gray2,
                          fontSize: 10,
                        ),
                      ),
                      SizedBox(width: 4.0,),
                      Text(
                        movieData.originalTitle,
                        style: TextStyle(
                          color: globals.gray2,
                          fontWeight: FontWeight.w500,
                          fontSize: 10,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 32,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      
                      InfoSquare('Ano:',DateTime.parse(movieData.releaseDate).year.toString()),
                      InfoSquare('Duração:',Duration(minutes: movieData.runtime).inHours.toString()+'h '+(movieData.runtime%60).toString()+'min'),
                    ],
                  ),
                  SizedBox(height: 12,),
                  Center(
                    child: Wrap(
                      spacing: 8.0,
                      runSpacing: 4.0,
                      alignment: WrapAlignment.center,
                      children: movieData.genres.map<Widget>((genre)=>
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(0xffe9ecef),),                 
                            borderRadius:  BorderRadius.all(Radius.circular(5.0)),
                          ),
                          child: Text(
                            genre.name.toUpperCase(),
                            style: TextStyle(
                              color: globals.gray2,
                              fontWeight: FontWeight.w600,
                              fontSize: 14,
                            ),
                          )
                        )
                      ).toList(),
                    ),
                  ),
                  SizedBox(height: 56,),
                  InfoLongDescription('Descrição',movieData.overview),
                  SizedBox(height: 40,),
                  Column(
                    children: [
                      InfoSquare('Orçamento:', NumberFormat.simpleCurrency().format(movieData.budget)),
                      SizedBox(height: 4,),
                      InfoSquare('Produtoras:',movieData.productionCompanies),
                    ],
                  ),            
                  SizedBox(height: 40,),
                  InfoLongDescription('Diretor',movieData.director),             
                  SizedBox(height: 32,),
                  InfoLongDescription('Elenco',movieData.cast),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}