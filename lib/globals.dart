library globals;

import 'dart:ui';

Color gray1 = Color(0xff343a40);
Color gray2 = Color(0xff5e6770);
Color gray2_2 = Color(0xff6d7070);//Referência do Figma define dois valores diferentes com mesmo nome
Color gray3 = Color(0xff868e96);
Color gray8 = Color(0xfff5f5f5);