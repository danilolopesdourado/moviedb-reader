import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:moviedb_reader/dataModels/dataModels.dart';

final String baseUrl = 'https://api.themoviedb.org/3/';
final String apiKey = '31c88f9bc31839ea593bffc2a1f4e872';
final String language = 'pt-BR';


Future<Map> fetchCredits(int id) async {
  final response = await http.get(    
    baseUrl + 
    'movie/' + id.toString() + '/credits'
    '?api_key=' + apiKey + 
    '&language=' + language
    );

  if (response.statusCode == 200) {
    int castSize = jsonDecode(response.body)['cast'].length;
    String names = jsonDecode(response.body)['cast'].getRange(0,min(castSize,5)).map((e) => e['name']).join(', ');
    Map directorObj = jsonDecode(response.body)['crew'].where((e)=> e['job']=='Director').first;
    return {
      'names':names,
      'director': directorObj['name'],
      };
  } else {
    throw Exception('Failed to load movie data');
  }
}

Future<Movie> fetchMovie(int id) async {
  final response = await http.get(    
    baseUrl + 
    'movie/' + id.toString() +
    '?api_key=' + apiKey + 
    '&language=' + language
    );

  if (response.statusCode == 200) {
    dynamic movieJson = jsonDecode(response.body);
    Map creditsInfo = await fetchCredits(id);
    movieJson['cast'] = creditsInfo['names'];
    movieJson['director'] = creditsInfo['director'];
    return  Movie.fromJson(movieJson);
  } else {
    throw Exception('Failed to load movie data');
  }
}

Future<Map> fetchMovies(String apiUrl) async {
  final response = await http.get(    
    baseUrl + 
    apiUrl + 
    '?api_key=' + apiKey + 
    '&language=' + language
    );

  if (response.statusCode == 200) {
    var movies = jsonDecode(response.body)['results'] as List;
    List<Movie> movieList = List();
    List<Genre> genreList = List();
    for (var movie in movies) {
      Movie movieData = await fetchMovie(movie['id']);
      movieList.add(movieData);
      genreList.addAll(movieData.genres.where((a) => genreList.every((b) => a.id != b.id)));
    }
    return {'movieList': movieList, 'genreList': genreList};
  } else {
    throw Exception('Failed to load the list of movies');
  }
}